import 'package:flutter/material.dart';

// This is the overall App of the year Winners display page 1.
class appWinnersPage1 extends StatefulWidget {
  const appWinnersPage1({Key? key}) : super(key: key);

  @override
  State<appWinnersPage1> createState() => _appWinnersPage1State();
}

class _appWinnersPage1State extends State<appWinnersPage1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Overall Winners Page 1"),
        elevation: 0,
        centerTitle: true,
      ),
      bottomNavigationBar: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, "/Dashboard");
          },
          tooltip: "Dashboard",
          backgroundColor: Colors.black,
          isExtended: true),
      body: GridView(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 25,
          mainAxisExtent: 300,
          mainAxisSpacing: 25,
          childAspectRatio: 20,
        ),
        children: [
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2021 Winner!

              Ambani Africa
              Tech Company
              A Gamified African 
              Language learning 
                    App.
              """,
              softWrap: true,
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2020 Winner!

                    EasyEquities
                Share Trading Company
              Share buying investment 
                        App
              """,
              softWrap: true,
              style: TextStyle(
                  color: Colors.black,
                  decorationColor: Colors.blue,
                  fontSize: 30),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2019 Winner!

              Naked Insurance
              Insuretec Startup
              Financial solutions App
              """,
              softWrap: true,
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2018 Winner!

              Khula ecosystem
              Supply Chain Company
              Connects emerging 
                farmers to the 
              formal marketplace.
              """,
              softWrap: true,
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ),
        ],
      ),
    );
  }
}

// This is the Overall App of the year Winners display page 2.
class appWinnersPage2 extends StatefulWidget {
  const appWinnersPage2({Key? key}) : super(key: key);

  @override
  State<appWinnersPage2> createState() => _appWinnersPage2State();
}

class _appWinnersPage2State extends State<appWinnersPage2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Overall Winners Page 2"),
        elevation: 0,
        centerTitle: true,
      ),
      body: GridView(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 25,
          mainAxisExtent: 300,
          mainAxisSpacing: 25,
          childAspectRatio: 20,
        ),
        children: [
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2017 Winner!

                    Shyft 
              Money app service
              Digital Wallet App.
              """,
              softWrap: true,
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2016 Winner!

                  Domestly
              Cleaning Company
              cleaning-on-demand 
                    App
              """,
              softWrap: true,
              style: TextStyle(
                  color: Colors.black,
                  decorationColor: Colors.blue,
                  fontSize: 30),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.blue.shade600,
            child: const Text(
              """2015 Winner!

                WormDrop
              Logistic Co.
              Courier service
              """,
              softWrap: true,
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ),
        ],
      ),
    );
  }
}

// This is the Overall App of the year Winners display page 3.
class appWinnersPage3 extends StatefulWidget {
  const appWinnersPage3({Key? key}) : super(key: key);

  @override
  State<appWinnersPage3> createState() => _appWinnersPage3State();
}

class _appWinnersPage3State extends State<appWinnersPage3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Overall Winners Page 3"),
        elevation: 0,
        centerTitle: true,
      ),
      body: GridView(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 25,
          mainAxisExtent: 300,
          mainAxisSpacing: 25,
          childAspectRatio: 20,
        ),
        children: [
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.yellow.shade600,
            child: const Text(
              """2014 Winner!

              LIVE Inspect
              Solutions company
              Data Capture App
              """,
              softWrap: true,
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.yellow.shade600,
            child: const Text(
              """2013 Winner!

                    SnapScan 
              Payment Gateway Co.
              Mobile Payment Solution 
                        App
              """,
              softWrap: true,
              style: TextStyle(
                  color: Colors.black,
                  decorationColor: Colors.blue,
                  fontSize: 30),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(16.0),
            elevation: 20,
            color: Colors.yellow.shade600,
            child: const Text(
              """2012 Winner!

                    FNB
                    Bank
              FNB banking app
              """,
              softWrap: true,
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ),
        ],
      ),
    );
  }
}

// The user profile of the App.
class userProfile extends StatefulWidget {
  const userProfile({Key? key}) : super(key: key);

  @override
  State<userProfile> createState() => _userProfileState();
}

class _userProfileState extends State<userProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Profile"),
        elevation: 0,
        centerTitle: true,
      ),
      bottomNavigationBar: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, "/Dashboard");
          },
          tooltip: "Dashboard",
          backgroundColor: Colors.black,
          isExtended: true),
      body: ListView(
        children: <Widget>[
          Container(
            height: 250,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.black, Colors.grey.shade700],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                stops: [0.5, 0.9],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey.shade700,
                      minRadius: 35.0,
                      child: Icon(
                        Icons.call,
                        size: 30.0,
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor: Colors.white70,
                      minRadius: 60.0,
                      child: CircleAvatar(
                        radius: 50.0,
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor: Colors.red.shade300,
                      minRadius: 35.0,
                      child: Icon(
                        Icons.message,
                        size: 30.0,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Siyabonga Maseko",
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Soccer Player/Logistician",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    "Email",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    "palmeiro.leonardo@gmail.com",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Divider(),
                ListTile(
                  title: Text(
                    "GitHub",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    "https://github.com/siyamaseko",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Divider(),
                ListTile(
                  title: Text(
                    "Linkedin",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    "www.linkedin.com/in/siyabonga-maseko-ncamane",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
