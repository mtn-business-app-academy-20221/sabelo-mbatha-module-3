import "package:flutter/material.dart";
import "package:sabelo_mbatha_module_3/m3File1.dart";
import "package:sabelo_mbatha_module_3/m3File2.dart";

void main() => runApp(MaterialApp(
      initialRoute: "/login",
      routes: {
        "/login": (context) => Login(),
        "/userProfile": (context) => userProfile(),
        "/Registration": (context) => Registration(),
        "/Dashboard": (context) => Dashboard(),
        "/appsPage1": (context) => appWinnersPage1(),
        "/appsPage2": (context) => appWinnersPage2(),
        "/appsPage3": (context) => appWinnersPage3(),
      },
    )
  );
